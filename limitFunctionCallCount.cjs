module.exports = function limitFunctionCallCount(fun,n) { //created and exported the function with parameters callback function and a number
    if (typeof fun!== 'function' || n<0 || n === undefined) { // throws an error for wrong inputs  
    throw new Error ('error');} 

    function f1(...args){ // created the function 'f1' which takes multiple arguments
        if (n>0) { // if 'n' is greater than zero then decrement by one and return the callback function with the given arguments.Otherwise return null
            n--
            return fun(...args)
        }
        else {return null}
    }
    return f1 // finally returning the f1
}