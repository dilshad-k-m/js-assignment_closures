module.exports = function cachefunction(fun){ // creating and exporting the cachefunction with a callback function as the parameter
    if (arguments.length ===0 || typeof arguments[0]!=='function') throw new Error('error') //checking for conditions for throwing the error
    let c={} // creating the cache object in the closure scope
    return function f1(...args) { // creating the function that takes multiple arguments
        if (c[args]==undefined) { // if the argument is not in the object then invoke the callback function and add it to the object
            c[args]=fun(...args) 
            console.log('updated cache') // returning the updated cache
            return c[args]
        }
        else {
            console.log('no changes added to cache') // returning the previous cache
            return c[args]
        }
    }
}
