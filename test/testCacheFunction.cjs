const x=require('../cacheFunction.cjs') // importing the cachefunction
const y=function double(...args) { // defining callback function
    return args*2
}
const z = x(y) // calling the cachefunction with passing callback function
console.log(z(2))
console.log(z(2))
console.log(z(3))