const x = require('../limitFunctionCallCount.cjs') // importing the function and storing to variable
function hello(n){ // defining the callback function
    return n
}
let t=x(hello,2)
console.log(t(3))
console.log(t(4))
console.log(t(5))
console.log(t(6))