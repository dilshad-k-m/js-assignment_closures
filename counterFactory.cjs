// created the function 'counterFactory' and exported
module.exports = function counterFactory() {
    let n=0 //declared a variable in the closure scope
    let o={increment : function(){return n+=1},decrement : function(){return n-=1}}//created an object which accessed and modified the closure scope variable
    return o //returning the object
}